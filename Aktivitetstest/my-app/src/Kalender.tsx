import { useQuery } from "@apollo/client";
import gql from "graphql-tag";
import React from "react";

interface IAktivitet {
  id: string;
  BokNr: string;
  BokadAv: string;
  FranTid: string;
  FranDatum: string;
  Plats: string;
  TillTid: string;
  Titel: string;
}

const Kalender = () => {
  // läs in en query som listar alla aktiviteter mellan två datum
  const { loading, error, data } = useQuery(
    gql`
      query aktiviteter($franDatum: date, $tillDatum: date) {
        Aktiviteter(where:{FranDatum:{_gte:$franDatum, _lte:$tillDatum}}){
          id
		      BokNr
		      BokadAv
		      FranTid
		      FranDatum
		      Plats
		      TillTid
		      Titel
        }
      }
    `,
    {
      variables: {
        franDatum: "2025-01-01T00:00:00Z",
        tillDatum: "2025-01-10T00:00:00Z",
      },
    }
  );
  console.log(data);
  return (
    <>
      <h1>Kalender</h1>
      {/* ev. felmeddelande */}
      {error?.message}
      {/* om det finns data så visar vi alla aktiviteter som hämtats */}
      <ul>
        {!loading &&
          data &&
          data.Aktiviteter.map((aktivitet: IAktivitet) => (
            <li key={aktivitet.id}>{`Aktivitet: ${aktivitet.BokNr} - ${aktivitet.Titel} Datum: ${aktivitet.FranDatum}`}</li>
          ))
          }
      </ul>
    </>
  );
};

export default Kalender;
