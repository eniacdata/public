import React from "react";
import "./App.css";
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  createHttpLink,
} from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
import Kalender from "./Kalender";

const API_KEY =
'jjaAvwJe5iuLnK9ej1BBvPzqv3wHvm88o7FKjeXde1mqcUY9Nnp9YoU74SrOGdCn';
const graphql_url =  `https://nearby-duck-89.hasura.app/v1/graphql`;
function App() {
    const httpLink = createHttpLink({
      uri: graphql_url,
    });

    const authLink = setContext((_, { headers }) => {
      return {
        headers: {
          ...headers,
          'x-hasura-admin-secret':API_KEY,
        },
      };
    });

    const client = new ApolloClient({
      link: authLink.concat(httpLink),
      cache: new InMemoryCache(),
    });

    return (
      <ApolloProvider client={client}>
        <div className="App">
          <Kalender />
        </div>
      </ApolloProvider>
    );
}

export default App;
